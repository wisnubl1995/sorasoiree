import { type ClassValue, clsx } from "clsx";
import { twMerge } from "tailwind-merge";

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs));
}

export const GeneratedPriceId = Math.floor(Math.random() * 90000) + 10000;

export const GetPriceId = async () => {
  const priceId = localStorage.getItem("priceId");
  return priceId ? JSON.parse(priceId) : 0;
};
