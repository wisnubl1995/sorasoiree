import axios from "axios";

export const CART_QUERY_KEY = "cart";

export const getCart = async () => {
  const storedCart = localStorage.getItem(CART_QUERY_KEY);
  return storedCart ? JSON.parse(storedCart) : { items: [] };
};

export const isPriceIdExist = async (priceId: any) => {
  const data = await axios
    .get(`${process.env.NEXT_PUBLIC_API_HOST}/checkouts?filters[priceId][$eq]=${priceId}`)
    .then((response) => response);

  if (data.data.data.length > 0) {
    return data.data.data[0].id;
  }
  return 0;
};

export const postCheckOutData = async (checkoutData: any) => {
  try {
    const response = await axios.post(
      `${process.env.NEXT_PUBLIC_API_HOST}/checkouts`,
      checkoutData
    );

    if (response.status == 200) {
      const checkoutItems = checkoutData.data.items.map((item: any) => ({
        data: {
          checkout_id: response.data.data.id,
          productName: item.productName,
          description: item.description,
          color: item.color,
          quantity: item.quantity,
          size: item.size,
          stock_id: item.stockId,
        },
      }));
      const promises = checkoutItems.map((checkoutItem: any) => {
        return axios.post(`${process.env.NEXT_PUBLIC_API_HOST}/checkout-items`, checkoutItem);
      });

      const resultItems = await Promise.all(promises);
      const returnValue = {
        checkoutData,
        resultItems,
      };
      return returnValue;
      // wa me here when success insert to db
    } else {
      throw new Error("Error posting checkout Items");
    }
  } catch (error) {
    console.error("Error posting checkOutData:", error);
    throw new Error("Error posting checkOutData");
  }
};

export const updateCheckOutData = async (checkoutData: any, id: any) => {
  try {
    // Update checkout table
    const response = await axios.put(
      `${process.env.NEXT_PUBLIC_API_HOST}/checkouts/${id}`,
      checkoutData
    );

    if (response.status == 200) {
      // get checkout-items table by checkout id
      const checkoutItemsData = await axios
        .get(
          `${process.env.NEXT_PUBLIC_API_HOST}/checkout-items?filters[checkout_id][$eq]=${response.data.data.id}`
        )
        .then((response) => response);

      // delete the checkout-item for updating later
      checkoutItemsData.data.data.map((checkoutItem: any) => {
        return axios.delete(
          `${process.env.NEXT_PUBLIC_API_HOST}/checkout-items/${checkoutItem.id}`,
          {
            data: {
              checkout_id: response.data.data.id,
              productName: checkoutItem.productName,
              description: checkoutItem.description,
              color: checkoutItem.color,
              quantity: checkoutItem.quantity,
              size: checkoutItem.size,
              stock_id: checkoutItem.stockId,
            },
          }
        );
      });

      // create new and send result
      const checkoutItems = checkoutData.data.items.map((item: any) => ({
        data: {
          checkout_id: response.data.data.id,
          productName: item.productName,
          description: item.description,
          color: item.color,
          quantity: item.quantity,
          size: item.size,
          stock_id: item.stockId,
        },
      }));

      const promises = checkoutItems.map((checkoutItem: any) => {
        return axios.post(`${process.env.NEXT_PUBLIC_API_HOST}/checkout-items`, checkoutItem);
      });

      const resultItems = await Promise.all(promises);
      const returnValue = {
        checkoutData,
        resultItems,
      };

      return returnValue;
    } else {
      throw new Error("Error updating checkout Items");
    }
  } catch (error) {
    console.error("Error updating checkOutData:", error);
    throw new Error("Error updating checkOutData");
  }
};
