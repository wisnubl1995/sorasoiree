import axios from "axios";
import { useQuery } from "react-query";

const fetchData = async (url: any) => {
  const { data, status, statusText, config, headers } = await axios
    .get(`${process.env.NEXT_PUBLIC_API_HOST}${url}`)
    .then((response) => response);
  return data;
};

export const GetLookbookById = (id: string) => {
  return useQuery(["lookbook", id], () => fetchData(`/lookbooks/${id}?populate=*`));
};

export const GetLookbooks = () => {
  return useQuery("lookbooks", () => fetchData(`/lookbooks?populate=*`));
};
