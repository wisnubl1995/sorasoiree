import axios from "axios";
import { useQuery } from "react-query";

const fetchData = async (url: any) => {
  const { data, status, statusText, config, headers } = await axios
    .get(`${process.env.NEXT_PUBLIC_API_HOST}${url}`)
    .then((response) => response);
  return data;
};

export const GetData = (slug: string) => {
  return useQuery("moreInformation", () => fetchData(`/${slug}`));
};

export const GetInfo = () => {
  return useQuery("additionalInformation", () => fetchData(`/additional-information`));
};
