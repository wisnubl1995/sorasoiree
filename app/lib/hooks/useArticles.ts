import axios from "axios";
import { useQuery } from "react-query";

const fetchData = async (url: any) => {
  const { data, status, statusText, config, headers } = await axios
    .get(`${process.env.NEXT_PUBLIC_API_HOST}${url}`)
    .then((response) => response);
  return data;
};

export const GetArticles = () => {
  const {
    data: articlesData,
    isLoading,
    status,
  } = useQuery("articles", () => fetchData(`/articles?populate=*`));

  const articles = articlesData?.data.map((v: any) => {
    const dateString = v.attributes.createdAt;
    const formattedDate = new Date(dateString).toLocaleDateString("en-GB", {
      day: "2-digit",
      month: "short",
      year: "numeric",
    });

    return {
      articleId: v.id,
      title: v.attributes.title,
      image: v.attributes.coverImage.data.attributes.url,
      contents: v.attributes.contents,
      date: formattedDate,
      author: v.attributes.author,
    };
  });

  return {
    isLoading,
    articles,
    status,
  };
};

export const GetArticlesById = (id: string) => {
  const { data, isLoading, status } = useQuery("article", () =>
    fetchData(`/articles/${id}?populate=*`)
  );
  const dateString = data?.data.attributes.createdAt;
  const formattedDate = new Date(dateString).toLocaleDateString("en-GB", {
    day: "2-digit",
    month: "short",
    year: "numeric",
  });
  const article = {
    title: data?.data.attributes.title,
    image: data?.data.attributes.coverImage.data.attributes.url,
    content: data?.data.attributes.contents,
    date: formattedDate,
    author: data?.data.attributes.author,
  };

  return {
    isLoading,
    article,
    status,
  };
};
