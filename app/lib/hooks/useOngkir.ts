import axios from "axios";
import { useQuery } from "react-query";
import { city, cost, province } from "../utils/rajaOngkir";

const fetchData = async (url: any) => {
  try {
    return await axios
      .get(`${process.env.NEXT_PUBLIC_RAJAONGKIR}${url}`, {
        headers: {
          key: "fb9f17a0bd82cd9456897c88616ad76b",
          "Content-Type": "application/json",
        },
      })
      .then((response) => response);
  } catch (error) {
    console.error(error);
  }
};

export const postData = async (url: any, body: any) => {
  const { origin, destination, weight, courier } = body;
  const formData = new FormData();
  formData.append("origin", origin);
  formData.append("destination", destination);
  formData.append("weight", weight);
  formData.append("courier", courier);

  try {
    const response = await axios.post(`${process.env.NEXT_PUBLIC_RAJAONGKIR}${url}`, formData, {
      headers: {
        key: "fb9f17a0bd82cd9456897c88616ad76b",
        "Content-Type": "multipart/form-data",
      },
    });
    return response?.data.rajaongkir.results[0].costs[1].cost[0].value;
  } catch (error) {
    console.error(error);
  }
};

export const GetProvince = () => {
  return useQuery("province", () => fetchData(`/province`));
};

export const GetCity = (provinceId: any) => {
  return useQuery(["city", provinceId], () => fetchData(`/city?province=${provinceId}`));
};

export const GetCost = (body: any) => {
  var data = JSON.stringify(body);
  return useQuery("cost", () => postData(`/cost`, data));
};
