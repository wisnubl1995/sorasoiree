import axios from "axios";
import { useQuery } from "react-query";

export type Products = {
  id: string;
  name: string;
  priceIDR: number;
  isBestSeller: boolean;
  productCategory: string;
  descriptions: string;
  color: string;
  productSize: string;
  productImage: string;
};

const fetchData = async (url: any) => {
  const { data, status, statusText, config, headers } = await axios
    .get(`${process.env.NEXT_PUBLIC_API_HOST}${url}`)
    .then((response) => response);
  return data;
};

export const GetProductsByStockId = (stockId: any) => {
  const {
    data: productData,
    isLoading,
    status,
  } = useQuery("product", () =>
    fetchData(
      `/product-stocks?populate[product][populate][0]=productCategory&populate[product][populate][1]=productMaterial&populate[product][populate][2]=productType&populate[productSizes]=*&populate[productColor]=*&populate[productSecondaryColor]=*&populate[productPictures]=*&filters[id][$eq]=${stockId}`
    )
  );
  const product = productData?.data.map((v: any) => {
    return {
      stockId: v?.id,
      productId: v?.attributes.product.data.id,
      productName: v?.attributes.product.data.attributes.productName,
      publishName: v?.attributes.product.data.attributes.publishName,
      productStock: v?.attributes.stock,
      priceIDR: v?.attributes.product.data.attributes.priceIDR,
      isBestSeller: v?.attributes.product.data.attributes.isBestSeller,
      productCategory: v?.attributes.product.data.attributes.productCategory.data?.attributes.name,
      productMaterial: v?.attributes.product.data.attributes.productMaterial.data?.attributes.name,
      productType: v?.attributes.product.data.attributes.productType?.data?.attributes.name,
      descriptions: v?.attributes.descriptions,
      color: v?.attributes.productColor.data.attributes.name,
      secondaryColor: v?.attributes.productSecondaryColor.data.attributes.name,
      productSize: v?.attributes.productSizes.data.attributes.name,
      productImage: v?.attributes.productPictures.data,
    };
  });
  return {
    isLoading,
    status,
    product: product,
  };
};

export const GetProducts = () => {
  const {
    data: productData,
    isLoading,
    status,
  } = useQuery("products", () =>
    fetchData(
      `/product-stocks?populate[product][populate][0]=productCategory&populate[product][populate][1]=productMaterial&populate[product][populate][2]=productType&populate[productSizes]=*&populate[productColor]=*&populate[productSecondaryColor]=*&populate[productPictures]=*`
    )
  );
  const products = productData?.data.map((v: any) => {
    return {
      stockId: v?.id,
      productId: v?.attributes.product.data.id,
      productName: v?.attributes.product.data.attributes.productName,
      publishName: v?.attributes.product.data.attributes.publishName,
      productStock: v?.attributes.stock,
      priceIDR: v?.attributes.product.data.attributes.priceIDR,
      isBestSeller: v?.attributes.product.data.attributes.isBestSeller,
      productCategory: v?.attributes.product.data.attributes.productCategory.data?.attributes.name,
      productMaterial: v?.attributes.product.data.attributes.productMaterial.data?.attributes.name,
      productType: v?.attributes.product.data.attributes.productType?.data?.attributes.name,
      descriptions: v?.attributes.descriptions,
      color: v?.attributes.productColor.data.attributes.name,
      secondaryColor: v?.attributes.productSecondaryColor.data.attributes.name,
      productSize: v?.attributes.productSizes.data.attributes.name,
      productImage: v?.attributes.productPictures.data,
    };
  });
  return {
    isLoading,
    status,
    products: products || [],
  };
};

export const GetNewProducts = () => {
  const currentDate = new Date();
  const isoDate = currentDate.toISOString();

  const {
    data: productData,
    isLoading,
    status,
  } = useQuery(
    "products",
    () =>
      fetchData(
        `/product-stocks?populate[product][populate][0]=productCategory&populate[product][populate][1]=productMaterial&populate[product][populate][2]=productType&populate[productSizes]=*&populate[productColor]=*&populate[productSecondaryColor]=*&populate[productPictures]=*`
      )
    // &filters[product][createdAt][$gte]=${isoDate}
  );
  const products = productData?.data.slice(0, 8).map((v: any) => {
    return {
      stockId: v?.id,
      productId: v?.attributes.product.data.id,
      productName: v?.attributes.product.data.attributes.productName,
      publishName: v?.attributes.product.data.attributes.publishName,
      productStock: v?.attributes.stock,
      priceIDR: v?.attributes.product.data.attributes.priceIDR,
      isBestSeller: v?.attributes.product.data.attributes.isBestSeller,
      productCategory: v?.attributes.product.data.attributes.productCategory.data?.attributes.name,
      productMaterial: v?.attributes.product.data.attributes.productMaterial.data?.attributes.name,
      productType: v?.attributes.product.data.attributes.productType?.data?.attributes.name,
      descriptions: v?.attributes.descriptions,
      color: v?.attributes.productColor.data.attributes.name,
      secondaryColor: v?.attributes.productSecondaryColor.data.attributes.name,
      productSize: v?.attributes.productSizes.data.attributes.name,
      productImage: v?.attributes.productPictures.data,
    };
  });
  return {
    isLoading,
    status,
    newProducts: products || [],
  };
};

export const GetBestProducts = () => {
  const {
    data: productData,
    isLoading,
    status,
  } = useQuery("products", () =>
    fetchData(
      `/product-stocks?populate[product][populate][0]=productCategory&populate[product][populate][1]=productMaterial&populate[product][populate][2]=productType&populate[productSizes]=*&populate[productColor]=*&populate[productSecondaryColor]=*&populate[productPictures]=*&filters[product][isBestSeller][$eq]=true`
    )
  );
  const products = productData?.data.slice(0, 8).map((v: any) => {
    return {
      stockId: v?.id,
      productId: v?.attributes.product.data.id,
      productName: v?.attributes.product.data.attributes.productName,
      publishName: v?.attributes.product.data.attributes.publishName,
      productStock: v?.attributes.stock,
      priceIDR: v?.attributes.product.data.attributes.priceIDR,
      isBestSeller: v?.attributes.product.data.attributes.isBestSeller,
      productCategory: v?.attributes.product.data.attributes.productCategory.data?.attributes.name,
      productMaterial: v?.attributes.product.data.attributes.productMaterial.data?.attributes.name,
      productType: v?.attributes.product.data.attributes.productType?.data?.attributes.name,
      descriptions: v?.attributes.descriptions,
      color: v?.attributes.productColor.data.attributes.name,
      secondaryColor: v?.attributes.productSecondaryColor.data.attributes.name,
      productSize: v?.attributes.productSizes.data.attributes.name,
      productImage: v?.attributes.productPictures.data,
    };
  });
  return {
    isLoading,
    status,
    bestProducts: products || [],
  };
};

export const GetProductPromo = () => {
  const { data, isLoading, status } = useQuery("productPromo", () =>
    fetchData("/product-promos?populate=*")
  );
  const promo = data?.data?.map((v: any) => {
    return {
      promoName: v.attributes.promoName,
      promoPicture: v.attributes.promoPicture,
      discount: v.attributes.discount,
      totalProduct: v.attributes.totalProduct,
      startDate: v.attributes.startDate,
      endDate: v.attributes.endDate,
      descriptions: v.attributes.descriptions,
      productNames: v.attributes.productNames,
      productCategories: v.attributes.productCategories,
      products: v.attributes.products,
    };
  });
  return {
    isLoading,
    status,
    promo: promo || [],
  };
};

export const GetProductCategories = () => {
  const { data, isLoading, status } = useQuery("productCategories", () =>
    fetchData("/product-categories?populate=*&pagination[pageSize]=12")
  );
  const categories = data?.data.map((v: any) => {
    return {
      id: v.id,
      name: v.attributes.name,
      categoryImage: v.attributes.categoryImage?.data[0]?.attributes,
    };
  });
  return { categories, isLoading, status };
};

export const GetProductByCategoryId = (id: any, page: number = 1) => {
  const { data, isLoading, status, isPreviousData } = useQuery(
    ["productByCategoryId", page],
    () =>
      fetchData(
        `/product-stocks?populate[product][populate][0]=productCategory&populate[product][populate][1]=productMaterial&populate[product][populate][2]=productType&populate[productSizes]=*&populate[productColor]=*&populate[productSecondaryColor]=*&populate[productPictures]=*&pagination[pageSize]=4${
          id ? `&filters[product][productCategory][id][$eq]=${id}` : ""
        }&pagination[page]=${page}`
      ),
    { staleTime: 0, keepPreviousData: true }
  );

  const products = data?.data.map((v: any) => {
    return {
      stockId: v?.id,
      productId: v?.attributes.product.data.id,
      productName: v?.attributes.product.data.attributes.productName,
      publishName: v?.attributes.product.data.attributes.publishName,
      productStock: v?.attributes.stock,
      priceIDR: v?.attributes.product.data.attributes.priceIDR,
      isBestSeller: v?.attributes.product.data.attributes.isBestSeller,
      productCategory: v?.attributes.product.data.attributes.productCategory.data?.attributes.name,
      productMaterial: v?.attributes.product.data.attributes.productMaterial.data?.attributes.name,
      productType: v?.attributes.product.data.attributes.productType?.data?.attributes.name,
      descriptions: v?.attributes.descriptions,
      color: v?.attributes.productColor.data.attributes.name,
      secondaryColor: v?.attributes.productSecondaryColor.data.attributes.name,
      productSize: v?.attributes.productSizes.data.attributes.name,
      productImage: v?.attributes.productPictures.data,
    };
  });

  return {
    isLoading,
    status,
    products: products || [],
    meta: data?.meta,
    isPreviousData: isPreviousData,
    data: data,
  };
};
