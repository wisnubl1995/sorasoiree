"use client";
import Navbar from "@/app/components/navbar";
import Footer from "./components/footer";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { GeneratedPriceId, GetPriceId } from "./lib/utils/utils";
import { useEffect } from "react";

export default function Template({ children }: { children: React.ReactNode }) {
  const queryClient = useQueryClient();

  const { data: priceId } = useQuery("priceId", GetPriceId, { staleTime: 24 * 60 * 60 * 1000 });

  const setGeneratedPriceId = useMutation(
    async (setId: number) => {
      if (typeof window !== "undefined" && window.localStorage) {
        localStorage.setItem("priceId", JSON.stringify(setId));
        localStorage.setItem("confirmOrder", JSON.stringify(false));
      }
      return setId;
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries("priceId");
      },
    }
  );

  useEffect(() => {
    if (priceId == 0) {
      setGeneratedPriceId.mutate(GeneratedPriceId);
    }
  }, [priceId]);

  useEffect(() => {
    const timer = setTimeout(() => {
      localStorage.removeItem("priceId");
      queryClient.invalidateQueries("priceId");
    }, 24 * 60 * 60 * 1000);

    return () => clearTimeout(timer);
  }, []);

  return (
    <div className="">
      <div className="max-w-7xl mx-auto">
        <Navbar />
        <div className="">{children}</div>
      </div>
      <Footer />
    </div>
  );
}
