import Image from "next/image";
import Link from "next/link";

type Props = {
  publishName: string;
  image: string;
  priceIDR: number;
  stockId: string;
};

const ProductCard = ({ stockId, image, publishName, priceIDR }: Props) => {
  return (
    <Link
      href={`/product/${stockId}`}
      className="mb-2 h-full w-full hover:shadow-sm justify-center justify-self-start"
    >
      <div className="w-auto overflow-hidden max-h-[300px] max-w-[300px]">
        <Image
          src={`${process.env.NEXT_PUBLIC_HOST}${image}`}
          alt="img"
          width={1000}
          height={1000}
          className="object-cover hover:scale-105 ease-in duration-100"
          priority
        />
      </div>
      <div className="text-[8px]">{publishName}</div>
      <div className="text-[8px] font-semibold">Rp. {priceIDR}</div>
    </Link>
  );
};

export default ProductCard;
