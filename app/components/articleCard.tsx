import Image from "next/image";
import Link from "next/link";
import React from "react";

type Props = { articleId: string; image: string; date: string; title: string; contents: any };

const ArticleCard = ({ articleId, image, date, title, contents }: Props) => {
  return (
    <Link href={`/article/${articleId}`}>
      <div className="border p-2 group flex flex-col gap-2 items-start justify-center md:w-full overflow-hidden hover:shadow-lg group-hover:border pb-1">
        <div className="w-full overflow-hidden">
          <Image
            className="aspect-square object-contain bg-gray-50 group-hover:scale-105 ease-in-outn duration-200"
            src={`${process.env.NEXT_PUBLIC_HOST}${image}`}
            alt=""
            width={500}
            height={500}
            priority
          />
        </div>
        <div className="text-[8px] tracking-tight leading-none">{date}</div>
        <div className="font-normal group-hover:underline underline-offset-2">{title}</div>
        <div className="line-clamp-2 md:line-clamp-2 h-full w-full">
          <div
            className="text-justify space-y-2 font-extralight text-[10px] lg:text-[8px] tracking-tight leading-tight"
            dangerouslySetInnerHTML={{ __html: contents }}
          />
        </div>
      </div>
    </Link>
  );
};

export default ArticleCard;
