import Image from "next/image";
import Link from "next/link";
import React, { Fragment } from "react";
import ProductCard from "./productCard";
import { Button } from "./ui/button";
import Loading from "../loading";

type Props = {
  title?: String;
  contents: any;
  isLoading: boolean;
};

const ProductSlider = ({ title, contents, isLoading }: Props) => {
  return (
    <div className="w-full h-full">
      <div className="flex justify-between items-center py-1">
        <h1 className="text-lg">{title}</h1>
        <Link
          href={"/products"}
          className="sm:hover:bg-white sm:hover:text-black sm:hover:border lg:hover:border-none lg:hover:text-white lg:hover:bg-black lg:hover:font-medium lg:hover:scale-105 text-[10px] text-white bg-black p-1"
        >
          View all
        </Link>
      </div>
      <div className="flex gap-2 overflow-hidden overflow-x-auto overflow-y-hidden min-h-[38vh] w-full">
        {contents?.map((v: any, i: any) => {
          return (
            <Fragment key={i}>
              {isLoading ? (
                <div className="lg:h-[88vh] h-[64vh] w-full flex items-center justify-center">
                  <Loading />
                </div>
              ) : (
                <div className="group">
                  <Link
                    href={`/product/${v.stockId}`}
                    className="mb-2 h-full w-full group-hover:shadow-sm"
                  >
                    <div className="w-52 overflow-hidden">
                      <Image
                        src={`${process.env.NEXT_PUBLIC_HOST}${v.productImage[0]?.attributes.url}`}
                        alt="img"
                        width={1000}
                        height={1000}
                        className="w-full object-cover group-hover:scale-105 ease-in duration-100"
                        priority
                      />
                    </div>
                    <div className="text-[8px] group-hover:underline underline-offset-2">{v.publishName}</div>
                    <div className="text-[8px] font-semibold">Rp. {v.priceIDR}</div>
                  </Link>
                </div>
              )}
            </Fragment>
          );
        })}
      </div>
    </div>
  );
};

export default ProductSlider;
