import { Card, CardContent, CardFooter, CardHeader, CardTitle } from "@/app/components/ui/card";
import { Button } from "./ui/button";
import Image from "next/image";
import Link from "next/link";
import { Fragment, useEffect, useMemo, useState } from "react";
import { MinusCircle } from "lucide-react";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { getCart } from "../lib/hooks/useCart";
import CheckOutDialog from "./checkOutDialog";
import { Dialog, DialogContent, DialogTrigger } from "@/app/components/ui/dialog";
import Inquiry from "./inquiry";

type Props = {
  cartData?: any;
  closeCart: any;
};

const CartPopover = ({ cartData, closeCart }: Props) => {
  const queryClient = useQueryClient();
  const [totalPrice, setTotalPrice] = useState(0);
  const [open, setOpen] = useState(false);
  const [resultCheckout, setResultCheckout] = useState<any>(null);
  const [showInquiry, setShowInquiry] = useState(false);

  const { data: cart } = useQuery("cart", getCart);

  const removeFromCart = useMutation(
    "cart",
    async (stockIdToRemove) => {
      const updatedCart = {
        items: cart.items.filter((item: any) => item.stockId !== stockIdToRemove),
      };
      localStorage.setItem("cart", JSON.stringify(updatedCart));
      return updatedCart;
    },
    {
      onSuccess: (updatedCart) => {
        queryClient.invalidateQueries("cart");
        queryClient.setQueryData("cart", updatedCart);
      },
    }
  );

  const checkoutData = cartData?.map((v: any, i: any) => {
    const subtotal = v.price * v.quantity;
    return (
      <Fragment key={v.stockId}>
        <Card className="flex w-full py-4 gap-2 pr-2">
          <Link
            href={`/product/${v.stockId}`}
            className="w-32 object-cover"
          >
            <Image
              src={v.image}
              alt="pic"
              width={80}
              height={80}
              priority
              className="w-auto h-auto object-cover mx-auto"
            />
          </Link>
          <div className="flex flex-col justify-between w-full">
            <div className="flex flex-col">
              <Link
                href={`/product/${v.stockId}`}
                className="font-medium hover:underline"
              >
                {v.productName}
              </Link>
              <div className="text-gray-500">{v.color}</div>
              <div className="text-gray-500">{v.size}</div>
            </div>
            <div className="flex justify-end items-end ">
              <div className="w-1/2 text-gray-500">Quantity: {v.quantity}</div>
              <div className="w-1/2 justify-end items-end text-right pt-4">
                {new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(
                  subtotal
                )}
              </div>
            </div>
          </div>
          <Button
            size={"icon"}
            onClick={() => {
              removeFromCart.mutate(v.stockId);
            }}
            className="border-none w-4 h-4"
          >
            <MinusCircle />
          </Button>
        </Card>
      </Fragment>
    );
  });

  const newTotalPrice = useMemo(() => {
    return cartData?.reduce((acc: any, item: any) => acc + item.price * item.quantity, 0);
  }, [cartData]);

  const formattedTotalPrice = useMemo(() => {
    return new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(
      newTotalPrice
    );
  }, [newTotalPrice]);

  useEffect(() => {
    setTotalPrice(newTotalPrice);
  }, [newTotalPrice]);

  const handleCloseDialog = () => {
    setOpen(false);
    closeCart();
  };

  return (
    <>
      {showInquiry && (
        <Inquiry
          data={resultCheckout}
          open={showInquiry}
          onClose={handleCloseDialog}
        />
      )}
      <Card>
        {totalPrice !== 0 ? (
          <CardHeader>
            <CardTitle className="pb-2">Your Cart</CardTitle>
            <hr />
            <>
              <CardContent className="max-h-96 overflow-y-scroll">{checkoutData}</CardContent>
              <CardFooter className="flex flex-col gap-4">
                <div className="uppercase flex justify-between w-full pt-2">
                  <span className="font-semibold">subtotal:</span>
                  <span>{formattedTotalPrice ?? 0}</span>
                </div>
                <Dialog
                  open={open}
                  onOpenChange={setOpen}
                >
                  <DialogTrigger asChild>
                    <Button
                      className="w-full"
                      variant={"color"}
                    >
                      Check Out
                    </Button>
                  </DialogTrigger>
                  <DialogContent className="w-80 lg:w-full">
                    <CheckOutDialog
                      cart={cart}
                      totalPrice={newTotalPrice}
                      onClose={handleCloseDialog}
                      setResultCheckout={setResultCheckout}
                      setShowInquiry={setShowInquiry}
                    />
                  </DialogContent>
                </Dialog>
              </CardFooter>
            </>
          </CardHeader>
        ) : (
          <div className="p-4">Your cart is empty</div>
        )}
      </Card>
    </>
  );
};

export default CartPopover;
