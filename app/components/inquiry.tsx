import React, { Fragment, useState } from "react";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/app/components/ui/dialog";
import Link from "next/link";
import { useQueryClient } from "react-query";

type Props = {
  data: any;
  open: any;
  onClose: any;
};

const Inquiry = ({ data, open, onClose }: Props) => {
  const totalPrice: number = data.totalPrice + data.cost;
  const queryClient = useQueryClient();
  const submitHandler = () => {
    const waLink =
      `https://wa.me/6282122920950?text=Halo%20Admin%20Sera%20Soire%0A` +
      `Saya ingin konfirmasi pesanan dengan detail:%0A%0A` +
      `Nama: ${encodeURIComponent(data.customerName)}%0A` +
      `Telpon: ${encodeURIComponent(data.phoneNumber)}%0A` +
      `Alamat: ${encodeURIComponent(data.customerAddress)} ${encodeURIComponent(
        data.city
      )}, ${encodeURIComponent(data.province)}%0A` +
      `Total Harga: Rp ${encodeURIComponent(totalPrice.toLocaleString("id-ID"))}%0A` +
      `Payment ID: ${encodeURIComponent(data.priceId)}`;

    window.open(waLink, "_blank");
    window.location.reload();
    localStorage.removeItem("cart");
    localStorage.removeItem("confirmOrder");
    localStorage.removeItem("inquiry");
    localStorage.removeItem("priceId");
    queryClient.invalidateQueries("cart");
    queryClient.invalidateQueries("confirmOrder");
    queryClient.invalidateQueries("inquiry");
    queryClient.invalidateQueries("priceId");
  };
  return (
    <Dialog
      open={open}
      onOpenChange={onClose}
    >
      <DialogContent>
        <DialogHeader>
          <DialogTitle>Your order was successfully created</DialogTitle>
        </DialogHeader>
        <div className="space-y-1 mt-2">
          <div className=" flex">
            <div className="w-1/2  flex">
              <span className="w-full">Customer Name</span> <span className="w-1/12 ">:</span>
            </div>
            <div className="w-1/2">{data.customerName}</div>
          </div>
          <div className=" flex">
            <div className="w-1/2  flex">
              <span className="w-full">Phone Number</span>
              <span className="w-1/12 ">:</span>
            </div>
            <div className="w-1/2">{data.phoneNumber}</div>
          </div>
          <div className="">
            <div className="w-1/2  flex">
              <span className="w-full">Customer Address</span>
              <span className="w-1/12 ">:</span>
            </div>
            <div className="">{`${data.customerAddress} ${data.city}, ${data.province}`}</div>
          </div>
          <div className=" flex">
            <div className="w-1/2  flex">
              <span className="w-full">Payment Id</span>
              <span className="w-1/12 ">:</span>
            </div>
            <div className="w-1/2">{data.priceId}</div>
          </div>
          <div className=" flex">
            <div className="w-1/2  flex">
              <span className="w-full">Price</span>
              <span className="w-1/12 ">:</span>
            </div>
            <div className="w-1/2">Rp. {data.totalPrice}</div>
          </div>
          <div className=" flex">
            <div className="w-1/2  flex">
              <span className="w-full">Delivery Fee</span>
              <span className="w-1/12 ">:</span>
            </div>
            <div className="w-1/2">Rp. {data.cost}</div>
          </div>
          <div className=" flex">
            <div className="w-1/2  flex">
              <span className="w-full">Total Price</span>
              <span className="w-1/12 ">:</span>
            </div>
            <div className="w-1/2">Rp. {totalPrice.toLocaleString("id-ID")}</div>
          </div>
          <div className="space-y-1">
            <div className="mt-4 mb-2">Your Items:</div>
            {data.items.map((item: any) => (
              <Fragment key={item.stockId}>
                <div className="flex gap-2">
                  <div className="w-[5%]">{item.quantity}</div>
                  <div className="w-[70%]">{item.productName}</div>
                  <div className="w-[40%]">{item.color}</div>
                  <div className="w-[25%]">{item.size}</div>
                </div>
              </Fragment>
            ))}
          </div>
          <div className="flex pt-5 justify-end">
            <button
              className="w-1/2 bg-gray-900 text-white text-center py-2 hover:bg-gray-800"
              onClick={submitHandler}
            >
              Confirm Order
            </button>
          </div>
        </div>
      </DialogContent>
    </Dialog>
  );
};

export default Inquiry;
