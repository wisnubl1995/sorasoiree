"use client";

import { GetProductCategories } from "@/app/lib/hooks/useProducts";
import Link from "next/link";
import { Button } from "./ui/button";
import {
  NavigationMenu,
  NavigationMenuContent,
  NavigationMenuIndicator,
  NavigationMenuItem,
  NavigationMenuLink,
  NavigationMenuList,
  NavigationMenuTrigger,
  NavigationMenuViewport,
} from "@/app/components/ui/navigation-menu";
import { Popover, PopoverContent, PopoverTrigger } from "@/app/components/ui/popover";
import CartPopover from "./cartPopover";
import { Dialog, DialogTrigger } from "@/app/components/ui/dialog";
import { Menu } from "lucide-react";
import MobileMenu from "./mobileMenu";
import { useQuery } from "react-query";
import { useState } from "react";
import Inquiry from "./inquiry";

type Props = {};

const Navbar = (props: Props) => {
  const [openCart, setOpenCart] = useState(false);
  const [open, setOpen] = useState(false);
  const { data: cart } = useQuery("cart", () => {
    const storedCart = localStorage.getItem("cart");
    return storedCart ? JSON.parse(storedCart) : null;
  });
  const { data: confirmOrder } = useQuery("confirmOrder", () =>
    localStorage.getItem("confirmOrder")
  );
  const { data: inquiry } = useQuery("inquiry", () => {
    const storedInquiry = localStorage.getItem("inquiry");
    return storedInquiry ? JSON.parse(storedInquiry) : {};
  });
  const cartCount = cart ? cart?.items.length : 0;

  const categories = GetProductCategories()?.categories?.map((v: any) => {
    return (
      <Link
        key={v.id}
        href={`/products/${v.id}`}
        className="hover:underline py-1 text-xs"
      >
        {v.name}
      </Link>
    );
  });

  const handleCloseCart = () => {
    setOpenCart(false);
  };

  const handleConfirmOrder = () => {
    localStorage.setItem("confirmOrder", "false");
  };

  return (
    <header className="sticky top-0 bg-white/70 backdrop-blur-sm shadow-sm z-10 flex w-full justify-between items-center lg:px-[120px] font-light">
      <nav className="hidden lg:flex w-1/3">
        <NavigationMenu className="justify-start gap-0">
          <NavigationMenuList className="w-1/3 justify-start">
            <Button className="text-xs border-none px-2 -ml-2">
              <Link href={"/products/all"}>Products</Link>
            </Button>
            <NavigationMenuItem className="py-1">
              <NavigationMenuTrigger>Categories</NavigationMenuTrigger>
              <NavigationMenuContent>
                <div className="p-2 h-full flex gap-4 py-2">
                  <div className="flex flex-col">{categories}</div>
                </div>
              </NavigationMenuContent>
            </NavigationMenuItem>
            <Button className="px-2 text-xs border-none">
              <Link href={"/articles"}>Articles</Link>
            </Button>
            <Button className="px-2 text-xs border-none">
              <Link href={"/lookbooks"}>Lookbooks</Link>
            </Button>
          </NavigationMenuList>
        </NavigationMenu>
      </nav>
      <nav className="lg:hidden w-1/3">
        <Popover>
          <PopoverTrigger asChild>
            <Button
              size={"icon"}
              className="border-none"
            >
              <Menu className="p-1" />
            </Button>
          </PopoverTrigger>
          <PopoverContent className="w-screen -mt-2">
            <MobileMenu />
          </PopoverContent>
        </Popover>
      </nav>

      <nav className="w-1/3 flex justify-center">
        <Link
          className="text-center font-medium text-lg uppercase"
          href={"/"}
        >
          SERASOIREE
        </Link>
      </nav>

      <nav className="w-1/3 flex justify-end">
        <Dialog
          open={open}
          onOpenChange={setOpen}
        >
          <DialogTrigger asChild>
            <Button
              className={`border-none bg-gray-100 text-xs ${
                confirmOrder !== "true" ? "hidden" : ""
              }`}
              onClick={() => handleConfirmOrder}
            >
              Confirm Order
            </Button>
          </DialogTrigger>
          {confirmOrder === "true" && (
            <Inquiry
              data={inquiry}
              open={open}
              onClose={setOpen}
            />
          )}
        </Dialog>

        <Popover
          open={openCart}
          onOpenChange={setOpenCart}
        >
          <PopoverTrigger asChild>
            <Button className="border-none text-xs lg:-mr-3 font-bold">Cart ( {cartCount} )</Button>
          </PopoverTrigger>
          <PopoverContent className="w-80 md:w-96 lg:mr-16">
            <CartPopover
              cartData={cart?.items}
              closeCart={handleCloseCart}
            />
          </PopoverContent>
        </Popover>
      </nav>
    </header>
  );
};

export default Navbar;
