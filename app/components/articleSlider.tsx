import { Fragment } from "react";
import ArticleCard from "./articleCard";

type Props = {
  content?: any;
};

const ArticleSlider = ({ content }: Props) => {
  return (
    <div className="w-full leading-none pt-0">
      <h1 className="text-center md:pb-2 text-lg">Articles</h1>
      <div className="grid md:grid-cols-3 sm:grid-cols-2 grid-cols-1 grid-rows-1 gap-4 md:gap-2">
        {content?.slice(0, 3).map((v: any, i: any) => {
          return (
            <Fragment key={i}>
              <ArticleCard
                articleId={v.articleId}
                image={v.image}
                date={v.date}
                title={v.title}
                contents={v.contents}
              />
            </Fragment>
          );
        })}
      </div>
    </div>
  );
};

export default ArticleSlider;
