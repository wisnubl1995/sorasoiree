import Link from "next/link";
import React, { Fragment } from "react";

type Props = {
  type?: string;
  crumbs?: any;
};

const Breadcrumb = ({ crumbs, type }: Props) => {
  return (
    <nav className="h-14 w-full flex flex-col justify-center items-center border-b">
      <h1 className="font-normal">{type}</h1>

      <li className="text-[8px] list-none">
        <Link
          href={"/"}
          className="hover:underline"
        >
          Home
        </Link>{" "}
        /{" "}
        {crumbs.map((crumb: any, index: any) => (
          <Fragment key={index}>
            <Link
              href={crumb.url}
              className="hover:underline"
            >
              {crumb.label}
            </Link>
            {index < crumbs.length - 1 && " / "}
          </Fragment>
        ))}
      </li>
    </nav>
  );
};

export default Breadcrumb;
