'use client'

import React from "react";
import { Card } from "./ui/card";
import Image from "next/image";
import Link from "next/link";
import { Button } from "./ui/button";
import { MinusCircle } from "lucide-react";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { getCart } from "../lib/hooks/useCart";

type Props = {
  productName: string;
  color: string;
  size: string;
  imageUrl: string;
  price: any;
  stockId: any;
  quantity: any;
};

const CheckoutData = ({ productName, color, size, imageUrl, price, stockId, quantity }: Props) => {
  const queryClient = useQueryClient();
  const { data: cart } = useQuery("cart", getCart);

  const removeFromCart = useMutation(
    "cart",
    async (stockIdToRemove) => {
      const updatedCart = {
        items: cart.items.filter((item: any) => item.stockId !== stockIdToRemove),
      };
      localStorage.setItem("cart", JSON.stringify(updatedCart));
      return updatedCart;
    },
    {
      onSuccess: (updatedCart) => {
        queryClient.invalidateQueries("cart");
        queryClient.setQueryData("cart", updatedCart);
      },
    }
  );

  return (
    <Card
      className="flex w-full py-4 gap-2 pr-2"
    >
      <Link
        href={`product/${stockId}`}
        className="w-32 object-cover"
      >
        <Image
          src={imageUrl}
          alt="pic"
          width={80}
          height={80}
          priority
          className="w-auto h-auto object-cover mx-auto"
        />
      </Link>
      <div className="flex flex-col justify-between w-full">
        <div className="flex flex-col">
          <Link
            href={`product/${stockId}`}
            className="font-medium hover:underline"
          >
            {productName}
          </Link>
          <div className="text-gray-500">{color}</div>
          <div className="text-gray-500">{size}</div>
        </div>
        <div className="flex justify-end items-end ">
          <div className="w-1/2 text-gray-500">Quantity: {quantity}</div>
          <div className="w-1/2 justify-end items-end text-right pt-4">
            {new Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(
              price * quantity
            )}
          </div>
        </div>
      </div>
      <Button
        size={"icon"}
        onClick={() => {
          removeFromCart.mutate(stockId);
        }}
        className="border-none w-4 h-4"
      >
        <MinusCircle />
      </Button>
    </Card>
  );
};

export default CheckoutData;
