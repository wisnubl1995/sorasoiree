import Link from "next/link";
import React from "react";
import { GetProductCategories } from "../lib/hooks/useProducts";

type Props = {};

const MobileMenu = (props: Props) => {
  const categories = GetProductCategories()?.categories?.map((v: any) => {
    return (
      <Link
        key={v.id}
        href={`/products/${v.id}`}
        className="hover:underline py-2 text-xs"
      >
        - {v.name}
      </Link>
    );
  });

  return (
    <div className="justify-start gap-0">
      <div className="w-full justify-start items-start flex flex-col">
        <Link
          href={"/products/all"}
          className="hover:underline py-2 px-2 text-xs w-full justify-start"
        >
          Products
        </Link>
        <div className="px-2">
          <div className="py-2">Categories:</div>
          <div className="px-2 h-full flex">
            <div className="flex flex-col">{categories}</div>
          </div>
        </div>
        <Link
          href={"/articles"}
          className="hover:underline py-2 px-2 text-xs w-full justify-start"
        >
          Articles
        </Link>
        <Link
          href={"/lookbooks"}
          className="hover:underline py-2 px-2 text-xs w-full justify-start"
        >
          Lookbook
        </Link>
      </div>
    </div>
  );
};

export default MobileMenu;
