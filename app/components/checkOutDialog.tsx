import React, { useState } from "react";
import { DialogHeader, DialogTitle } from "@/app/components/ui/dialog";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import * as z from "zod";
import { Form, FormControl, FormField, FormItem, FormMessage } from "./ui/form";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/app/components/ui/select";
import { Button } from "./ui/button";
import { Input } from "./ui/input";
import { Textarea } from "@/app/components/ui/textarea";
import { useToast } from "@/app/components/ui/use-toast";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { isPriceIdExist, postCheckOutData, updateCheckOutData } from "../lib/hooks/useCart";
import { ToastAction } from "./ui/toast";
import { useRouter } from "next/navigation";
import { GetCity, GetProvince, postData } from "../lib/hooks/useOngkir";
import Loading from "../loading";
import { GetPriceId } from "../lib/utils/utils";

interface Props {
  cart: any;
  totalPrice: any;
  onClose: any;
  setResultCheckout: any;
  setShowInquiry: any;
}

const phoneRegex = new RegExp(/^([+]?[\s0-9]+)?(\d{3}|[(]?[0-9]+[)])?([-]?[\s]?[0-9])+$/);

const formSchema = z.object({
  firstName: z.string().min(2, {
    message: "First name must be at least 3 characters.",
  }),
  lastName: z.string().min(2, {
    message: "First name must be at least 3 characters.",
  }),
  address: z.string().min(2, {
    message: "Address cannot be empty!",
  }),
  whatsappNumber: z
    .string()
    .min(10, "Invalid phone number!")
    .regex(phoneRegex, "Invalid phone number!"),
  province: z.string({
    required_error: "Please select a province.",
  }),
  townCity: z.string({
    required_error: "Please select a town city.",
  }),
});

const CheckOutDialog = ({
  cart,
  totalPrice,
  onClose,
  setResultCheckout,
  setShowInquiry,
}: Props) => {
  const router = useRouter();
  const queryClient = useQueryClient();
  const [provinceId, setProvinceId] = useState(null);
  const { toast } = useToast();

  const { mutate } = useMutation(postCheckOutData, {
    onSuccess: (data: any) => {
      localStorage.setItem("confirmOrder", "true");
      localStorage.setItem("inquiry", JSON.stringify(data?.checkoutData.data));
      queryClient.invalidateQueries("cart");
      queryClient.invalidateQueries("confirmOrder");
      queryClient.invalidateQueries("inquiry");
      setResultCheckout(data?.checkoutData.data);
      setShowInquiry(true);
    },
  });

  const { mutate: update } = useMutation(
    ([checkOutData, id]: [any, any]) => updateCheckOutData(checkOutData, id),
    {
      onSuccess: (data: any) => {
        localStorage.setItem("confirmOrder", "true");
        localStorage.setItem("inquiry", JSON.stringify(data?.checkoutData.data));
        queryClient.invalidateQueries("cart");
        queryClient.invalidateQueries("confirmOrder");
        queryClient.invalidateQueries("inquiry");
        setResultCheckout(data?.checkoutData.data);
        setShowInquiry(true);
      },
    }
  );

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      firstName: "",
      lastName: "",
      address: "",
      whatsappNumber: "",
    },
  });

  const { data: provinces } = GetProvince();
  const { data: cities } = GetCity(provinceId);
  const { data: priceId } = useQuery("priceId", GetPriceId);

  const handleProvinceChange = (selectedProvinceId: any) => {
    setProvinceId(selectedProvinceId);
  };

  async function onSubmit(values: z.infer<typeof formSchema>) {
    try {
      if (priceId != 0 || priceId != undefined) {
        const [provinceId, provinceName] = values.province.split("-");
        const [cityId, cityName] = values.townCity.split("-");

        const finalPrice = totalPrice + priceId;

        const totalWeight = cart.items.reduce((acc: any, item: any) => acc + item.quantity * 50, 0);

        const body = {
          origin: "151",
          destination: cityId,
          weight: totalWeight,
          courier: "jne",
        };

        const cost = await postData("/cost", body);

        const checkOutData = {
          data: {
            customerName: values.firstName + " " + values.lastName,
            customerAddress: values.address,
            provinceId: provinceId,
            province: provinceName,
            cityId: cityId,
            city: cityName,
            phoneNumber: values.whatsappNumber,
            totalPrice: totalPrice,
            priceId: priceId,
            finalPrice: finalPrice,
            cost: cost,
            items: cart.items.map((item: any) => ({
              productName: item.productName,
              description: item.description,
              color: item.color,
              quantity: item.quantity,
              size: item.size,
              stockId: item.stockId,
            })),
          },
        };

        if (totalPrice != 0) {
          const checkOutId = await isPriceIdExist(priceId);
          checkOutId != 0 ? update([checkOutData, checkOutId]) : mutate(checkOutData);
        } else {
          toast({
            variant: "destructive",
            title: "Ups! Looks like you don't add any of our products",
            description: "There was a problem with your request.",
            action: (
              <ToastAction
                altText="Try again"
                onClick={() => router.push("/products/all")}
              >
                Go add some products!
              </ToastAction>
            ),
          });
        }
      }
    } catch (error) {
      toast({
        variant: "destructive",
        title: "There was a problem with your request.",
        action: (
          <ToastAction
            altText="Try again"
            onClick={() => location.reload()}
          >
            Try to reload current tab
          </ToastAction>
        ),
      });
      console.error("Error posting CheckOutData", error);
    }
  }

  return (
    <div>
      <DialogHeader className="pb-6">
        <DialogTitle>Contact & Delivery</DialogTitle>
      </DialogHeader>
      <Form {...form}>
        <form
          onSubmit={form.handleSubmit(onSubmit)}
          className="space-y-4"
        >
          <div className="flex w-full gap-2">
            <FormField
              control={form.control}
              name="firstName"
              render={({ field }) => (
                <FormItem className="w-1/2">
                  <FormControl>
                    <Input
                      placeholder="First Name"
                      {...field}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="lastName"
              render={({ field }) => (
                <FormItem className="w-1/2">
                  <FormControl>
                    <Input
                      placeholder="Last Name"
                      {...field}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>

          <FormField
            control={form.control}
            name="whatsappNumber"
            render={({ field }) => (
              <FormItem>
                <Input
                  placeholder="WhatsApp Number"
                  {...field}
                />
                <FormMessage />
              </FormItem>
            )}
          />

          <div className="flex w-full gap-2">
            <FormField
              control={form.control}
              name="province"
              render={({ field }) => (
                <FormItem className="w-1/2">
                  <Select
                    onValueChange={(value) => {
                      handleProvinceChange(value);
                      field.onChange(value);
                    }}
                    defaultValue={field.value}
                  >
                    <FormControl>
                      <SelectTrigger>
                        <SelectValue placeholder="Province" />
                      </SelectTrigger>
                    </FormControl>
                    <SelectContent>
                      {provinces ? (
                        provinces.data.rajaongkir?.results.map((v: any, i: any) => {
                          return (
                            <SelectItem
                              key={v.province_id}
                              value={`${v.province_id}-${v.province}`}
                            >
                              {v.province}
                            </SelectItem>
                          );
                        })
                      ) : (
                        <Loading />
                      )}
                    </SelectContent>
                  </Select>
                  <FormMessage />
                </FormItem>
              )}
            />

            <FormField
              control={form.control}
              name="townCity"
              render={({ field }) => (
                <FormItem className="w-1/2">
                  <Select
                    onValueChange={(value) => {
                      // handleCityChange(value);
                      field.onChange(value);
                    }}
                    defaultValue={field.value}
                    disabled={!provinceId}
                  >
                    <FormControl>
                      <SelectTrigger>
                        <SelectValue placeholder="Town/City" />
                      </SelectTrigger>
                    </FormControl>
                    <SelectContent>
                      {cities ? (
                        cities.data.rajaongkir?.results.map((v: any) => (
                          <SelectItem
                            key={v.city_id}
                            value={`${v.city_id}-${v.city_name}`}
                          >
                            {v.city_name}
                          </SelectItem>
                        ))
                      ) : (
                        <SelectItem
                          value="loading"
                          disabled
                        >
                          Loading...
                        </SelectItem>
                      )}
                    </SelectContent>
                  </Select>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>

          <FormField
            control={form.control}
            name="address"
            render={({ field }) => (
              <FormItem>
                <Textarea
                  placeholder="Address"
                  {...field}
                />
                <FormMessage />
              </FormItem>
            )}
          />

          <Button
            type="submit"
            className="w-full"
            variant={"color"}
          >
            Submit
          </Button>
        </form>
      </Form>
    </div>
  );
};

export default CheckOutDialog;
