"use client";
import Link from "next/link";
import { GetInfo } from "../lib/hooks/useInformation";

const Footer = () => {
  const currentDate = new Date();
  const currentYear = currentDate.getFullYear();

  const { data } = GetInfo();

  return (
    <div className="bg-gray-900 text-gray-100 px-3">
      <div className="lg:px-[120px] max-w-7xl mx-auto">
        <div className="grid lg:grid-cols-2 grid-cols-2 gap-6 text-[8px] py-10">
          <div className="uppercase flex flex-col gap-2">
            <p className="text-[10px]">Serasoiree</p>
            <hr />
            <ul className="space-y-1 capitalize w-full flex flex-col">
              <Link
                href={"/moreInformation/about-us"}
                className="hover:underline underline-offset-4"
              >
                About Us
              </Link>
              <Link
                href={"/moreInformation/terms-and-condition"}
                className="hover:underline underline-offset-4"
              >
                Terms & Condition
              </Link>
              <Link
                href={"/moreInformation/privacy-policy"}
                className="hover:underline underline-offset-4"
              >
                Privacy Policy
              </Link>
              {/* <Link href={"/"} className="hover:underline underline-offset-4 lowercase">serasoiree@email.com</Link> */}
            </ul>
          </div>
          {/* <div className="uppercase flex flex-col gap-2">
          <p className="text-[10px]"> Help</p>
          <hr />
          <ul className="space-y-1 capitalize">
            <ul>Contact Us</ul>
            <ul>How to order</ul>
          </ul>
        </div> */}
          <div className="uppercase flex flex-col gap-2">
            <p className="text-[10px]">Contact Us</p>
            <hr />
            <ul className="space-y-1 capitalize flex flex-col">
              {data?.data?.attributes ? (
                <>
                  <Link
                    href={`data?.data?.attributes.instagram`}
                    target="_blank"
                    className="hover:underline underline-offset-4"
                  >
                    Instagram
                  </Link>
                  {/* <ul>Facebook</ul> */}
                  <Link
                    href={`data?.data?.attributes.tiktok`}
                    target="_blank"
                    className="hover:underline underline-offset-4"
                  >
                    Tiktok
                  </Link>
                  <Link
                    href={`data?.data?.attributes.shopee`}
                    target="_blank"
                    className="hover:underline underline-offset-4"
                  >
                    Shopee
                  </Link>
                </>
              ) : (
                <></>
              )}
            </ul>
          </div>
          {/* <div className="uppercase flex flex-col gap-2">
          <p className="text-[10px]"> NewsLetter</p>
          <hr />
          <ul className="space-y-1 capitalize">
            <label htmlFor="">you email:</label>
            <input
              className="w-full text-gray-900 p-1"
              placeholder="serasoiree@email.com"
            />
          </ul>
        </div> */}
        </div>
        <p className="text-center pb-4 text-[8px]">
          Copyright &copy; SERASOIREE All Right Reserved {currentYear}
        </p>
      </div>
    </div>
  );
};

export default Footer;
