"use client";
import Image from "next/image";
import Link from "next/link";
import { Fragment } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination, Navigation } from "swiper/modules";
import Loading from "../loading";

type Props = {
  contents: any;
  isLoading: boolean;
};

const ProductSlider = ({ contents, isLoading }: Props) => {
  return (
    <Swiper
      slidesPerView={2}
      spaceBetween={15}
      pagination={{
        clickable: true,
      }}
      breakpoints={{
        300: {
          slidesPerView: 2,
          spaceBetween: 5,
        },
        768: {
          slidesPerView: 3,
          spaceBetween: 10,
        },
      }}
      modules={[Pagination, Navigation]}
      navigation={true}
      className=""
    >
      {contents?.map((v: any, i: any) => {
        return (
          <Fragment key={i}>
            <SwiperSlide
              className="relative group"
              key={v.id}
            >
              <div className="absolute lg:bottom-12 bottom-4 text-white left-0 capitalize text-center text-lg tracking-[0.15em] backdrop-blur-[1px] bg-black/20 w-full py-4 z-10 group-hover:shadow-lg">
                <p className="p-0 m-0 leading-none">{v.name}</p>
                <p className="text-[8px] leading-none">collection</p>
              </div>

              <Link
                href={`/products/${v.id}`}
                className="object-cover w-96 overflow-hidden"
              >
                {isLoading ? (
                  <div className="h-full w-full flex items-center justify-center">
                    <Loading />
                  </div>
                ) : (
                  <Image
                    className="w-full h-full group-hover:scale-105 transition ease-out duration-150"
                    src={`${process.env.NEXT_PUBLIC_HOST}${v.categoryImage.url}`}
                    alt="category"
                    width={1000}
                    height={1000}
                    priority
                  />
                )}
              </Link>
            </SwiperSlide>
          </Fragment>
        );
      })}
    </Swiper>
  );
};

export default ProductSlider;
