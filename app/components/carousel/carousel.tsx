import Image from "next/image";
import { GetProductPromo } from "@/app/lib/hooks/useProducts";
import { Button } from "../ui/button";
import Link from "next/link";
import Loading from "@/app/loading";

import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import "./styles.css";
import { Navigation, Pagination, Autoplay } from "swiper/modules";

type Props = {
  data?: any;
};

const Carousel = ({ data }: Props) => {
  const { promo, isLoading } = GetProductPromo();
  return (
    <Swiper
      spaceBetween={30}
      autoplay={{
        delay: 10000,
        disableOnInteraction: true,
      }}
      pagination={{
        clickable: true,
      }}
      navigation={true}
      modules={[Autoplay, Pagination, Navigation]}
      className="w-full bg-gray-100"
    >
      {promo?.map((v: any, i: any) => {
        return (
          <SwiperSlide key={i}>
            <div className="w-full overflow-hidden relative">
              <div className="absolute top-[60%] lg:top-[65%] md:tracking-[0.3em] leading-none text-left lg:text-center text-white w-full overflow-hidden px-2 lg:px-0 flex items-center justify-center flex-col">
                <h1 className="text-2xl lg:text-5xl">{v?.promoName}</h1>
                <p
                  className="text-[10px] lg:text-xs font-thin md:mt-2"
                  dangerouslySetInnerHTML={{ __html: v?.descriptions }}
                />
                <Button className="w-16 h-6 md:mt-4 mt-2 text-[10px] md:w-20 md:h-6 border-white hover:border-black hover:bg-black">
                  <Link href={"/products/all"}>Shop Now</Link>
                </Button>
              </div>
              {isLoading ? (
                <div className="lg:h-[88vh] h-[64vh] w-full flex items-center justify-center">
                  <Loading />
                </div>
              ) : (
                <div className="overflow-hidden">
                  {v?.promoPicture.data ? (
                    <Image
                      className="w-full h-[90vh] object-cover"
                      src={`${process.env.NEXT_PUBLIC_HOST}${
                        v.promoPicture ? v?.promoPicture.data?.attributes.url : "/"
                      }`}
                      width={1000}
                      height={1000}
                      alt="Trend"
                      priority
                    />
                  ) : (
                    <div className="w-full h-full text-[12px]">failed to load picture</div>
                  )}
                </div>
              )}
            </div>
          </SwiperSlide>
        );
      })}
    </Swiper>
  );
};

export default Carousel;
