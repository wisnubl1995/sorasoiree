"use client";
import React, { Fragment } from "react";
import { GetLookbooks } from "../lib/hooks/useLookbooks";
import Loading from "../loading";
import Image from "next/image";
import Link from "next/link";

type Props = {};

const Lookbooks = (props: Props) => {
  const { data: lookbooks } = GetLookbooks();
  return (
    <div className="py-4">
      {!lookbooks ? (
        <Loading />
      ) : (
        <div className="grid grid-cols-1 md:grid-cols-2 gap-2">
          {lookbooks.data?.map((v: any) => {
            return (
              <Fragment key={v.id}>
                <Link
                  href={`/lookbook/${v.id}`}
                  className="group relative flex flex-col border p-2 transition-all duration-300 transform hover:shadow-lg hover:cursor-pointer"
                >
                  <div className="text-xl font-bold my-1 transition-all duration-150 group-hover:border-b-[0.5px]">
                    {v.attributes.title}
                  </div>
                  <div className="overflow-hidden">
                    <Image
                      src={`${process.env.NEXT_PUBLIC_HOST}${v.attributes.cover_picture.data.attributes.formats.medium.url}`}
                      alt=""
                      width={500}
                      height={500}
                      className="aspect-square object-contain bg-gray-100  hover:scale-105 ease-linear duration-150"
                      priority
                    />
                  </div>
                  <div className="line-clamp-2 h-12 md:line-clamp-2 md:h-14 w-full group-hover:border-b-[0.5px]">
                    <div
                      className="py-4 pb-16 text-justify space-y-2 font-extralight "
                      dangerouslySetInnerHTML={{ __html: v.attributes.descriptions }}
                    />
                  </div>
                </Link>
              </Fragment>
            );
          })}
        </div>
      )}
    </div>
  );
};

export default Lookbooks;
