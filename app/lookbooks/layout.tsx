import React from 'react'

type Props = {}

const Layout = ({ children }: { children: React.ReactNode }) => {
  return (
    <div className='px-4 lg:px-[120px] min-h-[82vh]'>{children}</div>
  )
}

export default Layout