"use client";
import Breadcrumb from "@/app/components/breadcrumb";
import ProductCard from "@/app/components/productCard";
import { Button } from "@/app/components/ui/button";
import { getCart } from "@/app/lib/hooks/useCart";
import { GetProducts, GetProductsByStockId } from "@/app/lib/hooks/useProducts";
import Loading from "@/app/loading";
import { X } from "lucide-react";
import Image from "next/image";
import { Fragment, useEffect, useMemo, useState } from "react";
import { useMutation, useQuery, useQueryClient } from "react-query";

type Props = {};

const SingleProducts = ({ params }: { params: { slug: string } }) => {
  const stockId = params.slug;
  const [imageState, setImageState] = useState(0);
  const [imagePreview, setImagePreview] = useState(false);
  const [item, countItem] = useState(1);

  const { product, isLoading } = GetProductsByStockId(stockId);
  const { products, isLoading: isProductsLoading } = GetProducts();
  const queryClient = useQueryClient();

  const publishName = product ? product[0].publishName : "";
  const crumbs = [
    { label: "Products", url: "/products/all" },
    { label: `${publishName}`, url: `/products/${stockId}` },
  ];

  const closeImagePreview = () => {
    setImagePreview(false);
  };

  const setCart = useMutation(
    "cart",
    async (newCart: any) => {
      localStorage.setItem("cart", JSON.stringify(newCart));
      return newCart;
    },
    {
      onSuccess: (newData: any) => {
        queryClient.setQueryData("cart", newData);
        queryClient.invalidateQueries("cart");
      },
    }
  );

  function convertHtmlToString(html: string): string {
    const doc = new DOMParser().parseFromString(html, "text/html");
    return doc.body.textContent || "";
  }

  const { data: cart } = useQuery("cart", getCart);

  const handleAddToCart = (data: any) => {
    const newData = {
      image: process.env.NEXT_PUBLIC_HOST + data.productImage[0]?.attributes.formats.medium.url,
      productName: data.publishName,
      color: data.color + " " + data.secondaryColor,
      size: data.productSize,
      price: data.priceIDR,
      stockId: data.stockId,
      quantity: item,
      description: convertHtmlToString(data.descriptions),
    };

    if (cart && cart.items) {
      const existingItemIndex = cart.items.findIndex(
        (item: any) => item.stockId === newData.stockId
      );

      if (existingItemIndex !== -1) {
        const updatedCart = {
          ...cart,
          items: [
            ...cart.items.slice(0, existingItemIndex),
            newData,
            ...cart.items.slice(existingItemIndex + 1),
          ],
        };
        setCart.mutate(updatedCart);
        return;
      }
    }

    const newCart = {
      ...(cart || { items: [] }),
      items: [...(cart?.items || []), newData],
    };
    setCart.mutate(newCart);
  };

  useEffect(() => {
    const handleKeyDown = (event: any) => {
      if (event.key === "Escape") {
        closeImagePreview();
      }
    };
    window.addEventListener("keydown", handleKeyDown);
    return () => {
      window.removeEventListener("keydown", handleKeyDown);
    };
  }, []);

  return (
    <div>
      <Breadcrumb
        crumbs={crumbs}
        type="Product"
      />
      <div className="lg:px-[120px] py-2 px-3">
        {isLoading ? (
          <Loading />
        ) : (
          <>
            {product?.map((v: any, i: any) => {
              return (
                <Fragment key={v.stockId}>
                  <div className="flex flex-col md:flex-row w-full gap-6 pb-8">
                    <div className="md:w-1/2 flex gap-2">
                      <div className="flex flex-col gap-2 overflow-y-auto h-96 w-max scroll-smooth pl-px pr-2">
                        {v.productImage?.map((images: any, i: any) => {
                          return (
                            <Image
                              key={i}
                              className={`w-24 object-cover hover:cursor-pointer ease-in-out duration-150 ${
                                imageState == i ? "border border-black" : "hover:scale-105"
                              }`}
                              src={`${process.env.NEXT_PUBLIC_HOST}${images.attributes.formats.small.url}`}
                              alt="productImage"
                              width={800}
                              height={800}
                              priority
                              onClick={() => {
                                setImageState(i);
                              }}
                            />
                          );
                        })}
                      </div>
                      <div className="overflow-hidden">
                        <Image
                          className="object-contain h-96 w-96 bg-gray-50 mx-auto hover:cursor-pointer hover:scale-105 ease-in-out duration-150"
                          src={`${process.env.NEXT_PUBLIC_HOST}${v.productImage[imageState]?.attributes.formats.medium.url}`}
                          alt="productImage"
                          width={800}
                          height={800}
                          priority
                          onClick={() => setImagePreview(true)}
                        />
                      </div>
                      {imagePreview && (
                        <div className="fixed inset-0 flex items-center justify-center z-50">
                          <div
                            className="fixed inset-0 bg-black opacity-80"
                            onClick={closeImagePreview}
                          ></div>
                          <div className="relative z-50">
                            <Image
                              className="object-contain h-screen"
                              src={`${process.env.NEXT_PUBLIC_HOST}${v.productImage[imageState]?.attributes.url}`}
                              alt="productImage"
                              width={800}
                              height={800}
                              priority
                            />
                            <Button
                              size={"icon"}
                              className="absolute top-4 right-4 text-white text-xl cursor-pointer"
                              onClick={closeImagePreview}
                            >
                              <X />
                            </Button>
                          </div>
                        </div>
                      )}
                    </div>
                    <div className="md:w-1/2 flex flex-col gap-1">
                      <div className="flex flex-col justify-between h-full">
                        <div className="">
                          <h1 className="pb-2 text-xl font-normal">{v.publishName}</h1>
                          <strong className="font-semibold pb-4">Rp {v.priceIDR} </strong>
                          <div className="flex gap-4 md:gap-0 justify-start md:flex-col pt-4 md:pt-0 pb-2 md:pb-0">
                            <div className="space-y-2 md:py-2">
                              <p className="text-[#6F6F6F] capitalize font-normal md:pt-4">size:</p>
                              <button className="border border-black p-2 block">
                                {v.productSize}
                              </button>
                            </div>
                            <div className="space-y-2 md:py-2">
                              <p className="text-[#6F6F6F] capitalize font-normal">color:</p>
                              <div className="flex items-center gap-2">
                                <button className="border border-black p-2 block">
                                  <p className="">
                                    {v.color} {v.secondaryColor}
                                  </p>
                                </button>
                              </div>
                            </div>
                            <div className="space-y-2 md:py-1">
                              <p className="text-[#6F6F6F] capitalize font-normal">type:</p>
                              <div className="flex items-center gap-2">
                                <button className="border border-black p-2 block">
                                  <p className="">{v.productType}</p>
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="capitalize font-normal">
                          <p className="text-[#6F6F6F] capitalize font-normal py-3">Quantity:</p>
                          <div className="flex flex-col gap-4">
                            <div className="flex gap-2">
                              <div className="flex p-0 border border-black h-full">
                                <Button
                                  className="border-none"
                                  onClick={() => {
                                    if (item > 1) countItem(item - 1);
                                    else countItem(1);
                                  }}
                                >
                                  -
                                </Button>
                                <p className="w-8 h-8 flex items-center justify-center">{item}</p>
                                <Button
                                  className="border-none"
                                  onClick={() => countItem(item + 1)}
                                >
                                  +
                                </Button>
                              </div>
                              <Button
                                className="w-full"
                                onClick={() => handleAddToCart(v)}
                              >
                                Add to Cart
                              </Button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="">
                    <h1 className="font-normal capitalize mb-2 border-b border-black/20">
                      descriptions
                    </h1>
                    <p
                      className="pb-6 border-black/20 border-b-[1px]"
                      dangerouslySetInnerHTML={{ __html: v.descriptions }}
                    />
                  </div>
                </Fragment>
              );
            })}
          </>
        )}
        <div className="md:py-6 py-3">
          {isProductsLoading ? (
            <Loading />
          ) : (
            <div>
              <h1 className="leading-10 font-normal text-base">You Might also like</h1>
              <div className="flex overflow-y-auto gap-4">
                {products.map((v: any) => {
                  return (
                    <ProductCard
                      key={v.stockId}
                      publishName={v.publishName}
                      image={v.productImage[0]?.attributes.url}
                      priceIDR={v.priceIDR}
                      stockId={v.stockId}
                    />
                  );
                })}
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default SingleProducts;
