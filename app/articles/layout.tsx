import type { MDXComponents } from "mdx/types";

const MDXLayout = ({ children }: { children: React.ReactNode }) => {
  return <div>{children}</div>;
};

export default MDXLayout;
