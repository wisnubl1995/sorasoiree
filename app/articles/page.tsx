"use client";
import Breadcrumb from "../components/breadcrumb";
import { GetArticles } from "../lib/hooks/useArticles";
import Loading from "../loading";
import { Fragment } from "react";
import ArticleCard from "../components/articleCard";

type Props = {};

const Articles = (props: Props) => {
  const { articles, isLoading, status } = GetArticles();

  const crumbs = [{ label: "Articles", url: "/articles" }];

  return (
    <div className="">
      <Breadcrumb
        crumbs={crumbs}
        type="Article"
      />
      <div
        className={`p-2 lg:px-[120px] lg:pt-6 lg:pb-6 grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-4 justify-items-stretch place-content-between ${
          isLoading ? "flex" : "grid"
        }`}
      >
        {isLoading ? (
          <Loading />
        ) : (
          articles?.map((v: any, i: any) => {
            return (
              <Fragment key={i}>
                <ArticleCard
                  articleId={v.articleId}
                  image={v.image}
                  date={v.date}
                  title={v.title}
                  contents={v.contents}
                />
                <ArticleCard
                  articleId={v.articleId}
                  image={v.image}
                  date={v.date}
                  title={v.title}
                  contents={v.contents}
                />
              </Fragment>
            );
          })
        )}
      </div>
    </div>
  );
};

export default Articles;
