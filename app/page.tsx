"use client";
import { GetProductCategories, GetNewProducts, GetBestProducts } from "@/app/lib/hooks/useProducts";
import ProductSlider from "./components/productSlider";
import CategorySlider from "./components/categorySlider";
import ArticleSlider from "./components/articleSlider";
import Carousel from "./components/carousel/carousel";
import { GetArticles } from "./lib/hooks/useArticles";

export default function Home() {
  const { newProducts, isLoading: isNewProductLoading } = GetNewProducts();
  const { bestProducts, isLoading: isBestProductLoading } = GetBestProducts();
  const { categories, isLoading: isCategoryLoading } = GetProductCategories();
  const { articles, isLoading: isArticleLoading } = GetArticles();

  return (
    <div className="mb-4">
      <div className="pb-8">
        <Carousel />
      </div>
      <div className="lg:px-[120px] px-3 flex flex-col gap-8">
        <CategorySlider
          contents={categories}
          isLoading={isCategoryLoading}
        />
        <ProductSlider
          title={"New Arrival"}
          contents={newProducts}
          isLoading={isNewProductLoading}
        />
        <ProductSlider
          title={"Best Products"}
          contents={bestProducts}
          isLoading={isBestProductLoading}
        />
        <ArticleSlider content={articles} />
      </div>
    </div>
  );
}
