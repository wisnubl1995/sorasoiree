"use client";
import { GetData } from "@/app/lib/hooks/useInformation";
import Loading from "@/app/loading";
import React from "react";

type Props = {};

const MoreInformation = ({ params }: { params: { slug: string } }) => {
  const slugParam = params.slug;
  const { data, isLoading } = GetData(slugParam);

  return (
    <div>
      {isLoading ? (
        <Loading />
      ) : (
        <>
          <div className="font-bold text-center w-full text-3xl py-4">
            {data?.data.attributes.title}
          </div>
          <div
            className="py-4 pb-16 min-h-[64vh] text-justify space-y-2"
            dangerouslySetInnerHTML={{ __html: data?.data.attributes.content }}
          />
        </>
      )}
    </div>
  );
};

export default MoreInformation;
