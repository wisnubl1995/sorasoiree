"use client";
import React from "react";

type Props = {};

const Layout = ({ children }: { children: React.ReactNode }) => {
  return <div className="lg:px-[120px] px-4">{children}</div>;
};

export default Layout;
