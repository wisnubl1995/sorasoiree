"use client";
import { GetLookbookById } from "@/app/lib/hooks/useLookbooks";
import Loading from "@/app/loading";
import Image from "next/image";
import React, { Fragment, useState } from "react";

type Props = {};

const Lookbook = ({ params }: { params: { slug: string } }) => {
  const lookbookId = params.slug;
  const { data: lookbook } = GetLookbookById(lookbookId);
  const [imageState, setImageState] = useState(0);
  return (
    <div className="py-2">
      {!lookbook ? (
        <Loading />
      ) : (
        <div className="">
          <Image
            className="w-full h-[80vh] object-contain bg-gray-100"
            src={`${process.env.NEXT_PUBLIC_HOST}${lookbook.data.attributes.cover_picture.data.attributes.formats.large.url}`}
            width={1000}
            height={1000}
            alt="Session Picture"
            priority
          />
          <div className="text-xl font-bold my-1 transition-all duration-150 group-hover:border-b-[0.5px]">
            {lookbook.data.attributes.title}
          </div>
          <div className="flex gap-2">
            <div className="overflow-hidden">
              <Image
                className="border object-contain w-96 h-96 bg-gray-100"
                src={`${process.env.NEXT_PUBLIC_HOST}${lookbook.data?.attributes.session_pictures?.data[imageState]?.attributes.formats.medium.url}`}
                alt="productImage"
                width={500}
                height={500}
                priority
              />
            </div>
            <div className="w-full px-2 h-96 overflow-x-auto">
              <div
                className="text-justify space-y-2 font-extralight "
                dangerouslySetInnerHTML={{ __html: lookbook.data.attributes.descriptions }}
              />
            </div>
          </div>
          <div className="flex overflow-y-hidden my-2">
            {lookbook.data?.attributes.session_pictures?.data.map((v: any, i: any) => {
              return (
                <Fragment key={v.id}>
                  <Image
                    className="w-80 h-80 object-contain border bg-gray-100"
                    src={`${process.env.NEXT_PUBLIC_HOST}${v.attributes.formats.medium.url}`}
                    width={500}
                    height={500}
                    alt="Session Picture"
                    priority
                    onClick={() => setImageState(i)}
                  />
                </Fragment>
              );
            })}
          </div>
        </div>
      )}
    </div>
  );
};

export default Lookbook;
