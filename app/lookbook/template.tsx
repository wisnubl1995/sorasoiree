export default function Template({ children }: { children: React.ReactNode }) {
  return <div className="bg-gray-100 px-4 lg:px-[120px] min-h-[64vh]">{children}</div>;
}
