"use client";
import React, { Fragment } from "react";
import { ScrollArea } from "@/app/components/ui/scroll-area";
import { GetProductCategories } from "../lib/hooks/useProducts";
import Breadcrumb from "../components/breadcrumb";
import Link from "next/link";

type Props = {};

const Layout = ({ children }: { children: React.ReactNode }) => {
  const { categories } = GetProductCategories();

  const crumbs = [{ label: "Products", url: "/products/all" }];

  const category = categories?.map((v: any) => {
    return (
      <Fragment key={v.id}>
        <div className="">
          <Link
            key={v.id}
            href={`/products/${v.id}`}
            className="hover:underline text-[8px]"
          >
            {v.name}
          </Link>
        </div>
      </Fragment>
    );
  });

  return (
    <div className="">
      <Breadcrumb
        crumbs={crumbs}
        type="Product"
      />
      <hr className="mb-4" />
      <div className="flex gap-2 w-full static lg:px-[120px] px-4">
        <div className="hidden lg:w-1/4 lg:flex flex-col gap-2">
          <div className="flex lg:flex-col">
            <div className="font-semibold pb-1">Categories</div>
            <div className="text-xs pb-6">{category}</div>
          </div>
        </div>
        <ScrollArea className="w-full h-full">{children}</ScrollArea>
      </div>
    </div>
  );
};

export default Layout;
