"use client";
import { GetProductByCategoryId } from "@/app/lib/hooks/useProducts";
import { Fragment, useState } from "react";
import Loading from "@/app/loading";
import ProductCard from "@/app/components/productCard";
import { Button } from "@/app/components/ui/button";

type Props = {};

const Categories = ({ params }: { params: { slug: string } }) => {
  const [pageState, setPageState] = useState(1);
  const id = params.slug == "all" ? "" : params.slug;
  const { products, isLoading, isPreviousData, meta } = GetProductByCategoryId(id, pageState);

  const pageCount = meta?.pagination.pageCount;
  const currentPage = meta?.pagination.page;
  const pagesNumber = [];
  for (let index = 1; index <= pageCount; index++) {
    pagesNumber.push(index);
  }

  return (
    <div className="flex flex-col justify-between ">
      <div className="min-h-[64vh] pb-2 grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 justify-items-stretch place-content-between gap-2">
        {products?.map((v: any) => {
          return (
            <Fragment key={v.stockId}>
              {isLoading ? (
                <div className="lg:h-[88vh] h-[64vh] w-full flex items-center justify-center">
                  <Loading />
                </div>
              ) : (
                <ProductCard
                  publishName={v.publishName}
                  image={v.productImage[0]?.attributes.url}
                  priceIDR={v.priceIDR}
                  stockId={v.stockId}
                />
              )}
            </Fragment>
          );
        })}
      </div>
      {products && products.length > 0 && (
        <div className="w-full h-14 flex justify-center items-center">
          <Button
            onClick={() => setPageState((prevPage) => (prevPage > 1 ? prevPage - 1 : prevPage))}
            className="text-xs w-1/6 border-none flex justify-center"
            disabled={pageState === 1}
          >
            Previous
          </Button>
          <div className="w-1/8 flex justify-center">
            {pagesNumber?.map((v, i) => {
              return (
                <Button
                  key={i}
                  className={`border-none w-6 p-2 self-center ${pageState == v ? "font-bold" : ""}`}
                  onClick={() => setPageState(v)}
                >
                  {v}
                </Button>
              );
            })}
          </div>
          <Button
            onClick={() => {
              if (currentPage < pageCount) setPageState((prevPage) => prevPage + 1);
            }}
            className="text-xs w-1/6 border-none flex justify-center"
            disabled={currentPage === pageCount}
          >
            Next
          </Button>
        </div>
      )}
    </div>
  );
};

export default Categories;
