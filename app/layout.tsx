import type { Metadata } from "next";
import "./globals.css";
import { Hanken_Grotesk } from "next/font/google";
import ReactQueryProvider from "./reactQuery-provider";
import { Toaster } from "@/app/components/ui/toaster";

const roboto = Hanken_Grotesk({
  subsets: ["latin"],
  display: "swap",
});

export const metadata: Metadata = {
  title: "Serasoiree",
  description: "",
};

export default function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html lang="en">
      <body
        className={`${roboto.className} font-light text-xs tracking-widest bg-background antialiased`}
      >
        <ReactQueryProvider>{children}</ReactQueryProvider>
        <Toaster />
      </body>
    </html>
  );
}
