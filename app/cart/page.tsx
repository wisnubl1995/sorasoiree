"use client";

import React from "react";
import Breadcrumb from "../components/breadcrumb";
import CheckoutData from "../components/checkoutData";
import { getCart } from "../lib/hooks/useCart";
import { useQuery } from "react-query";

type Props = {};

const Cart = (props: Props) => {
  const { data: cart } = useQuery("cart", getCart);
  const crumb = [{ label: "Order", url: "/cart" }];

  return (
    <div className="">
      <Breadcrumb
        crumbs={crumb}
        type="Cart"
      />
      {/* <div className="px-[120px] min-h-[80vh]">
        <div className="w-96">
          {cart?.items.map((v: any) => {
            return (
              <CheckoutData
                key={v.stockId}
                productName={v.publishName}
                color={v.color}
                imageUrl={v.image}
                price={v.price * v.quantity}
                size={v.size}
                stockId={v.stockId}
                quantity={v.quantity}
              />
            );
          })}
        </div>
      </div> */}
    </div>
  );
};

export default Cart;
