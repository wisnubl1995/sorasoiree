"use client";
import Image from "next/image";
import Breadcrumb from "../../components/breadcrumb";
import { GetArticlesById } from "../../lib/hooks/useArticles";
import Loading from "../../loading";

type Props = {};

const Article = ({ params }: { params: { slug: string } }) => {
  const articleId = params.slug;
  const { article, isLoading, status } = GetArticlesById(articleId);
  const articleTitle = article ? article.title : "";

  const crumbs = [
    { label: "Articles", url: "/articles" },
    { label: `${articleTitle}`, url: `/products/${articleId}` },
  ];

  return (
    <div className="h-full">
      <Breadcrumb
        crumbs={crumbs}
        type="Article"
      />
      <div className="px-2 md:px-6 lg:px-[120px] py-2">
        {isLoading ? (
          <Loading />
        ) : (
          <>
            <div className="flex justify-between text-[8px] md:text-xs w-full">
              <span className="font-semibold">Author: {article.author}</span>
              <span className="font-light">{article.date}</span>
            </div>
            <h1 className="text-center text-2xl md:text-6xl font-semibold pb-2">{article.title}</h1>
            <div className="w-full overflow-hidden md:py-2 ">
              <Image
                className="mx-auto object-contain w-full h-[480px]"
                src={`${process.env.NEXT_PUBLIC_HOST}${article.image}`}
                alt=""
                width={800}
                height={800}
                priority
              />
            </div>
            <div className="py-4 md:py-8 md:pb-10 max-w-2xl mx-auto">
              <div
                className="text-justify space-y-3 lg:space-y-4 font-extralight text-[10px] lg:text-base tracking-tight leading-tight"
                dangerouslySetInnerHTML={{ __html: article.content }}
              />
            </div>
          </>
        )}
      </div>
    </div>
  );
};

export default Article;
